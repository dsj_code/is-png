# is-png

## 简介
判断图片是否为png格式

![img.png](img.png)

## 下载安装
```shell
npm install is-png
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

1. 生成isPng
```  
import {isPng} from 'is-png';
```
2. 判断图片格式
   ```  
   private aboutToAppear() {  
        resourceManager.getResourceManager().then(manager=>{
      manager.getMedia($r("app.media.fixture").id).then(value =>{
        this.isPng= isPng(value);
        console.info("image png:"+this.isPng);
      })
    })
    resourceManager.getResourceManager().then(manager=>{
      manager.getMedia($r("app.media.fixtur").id).then(value =>{
        this.isPng2= isPng(value);
        console.info("image png:"+this.isPng2);
      })
    })
   }
   ```
## 接口说明
```
 图片是否为png格式  isPng(buffer)
```

## 兼容性
支持 OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- is-png
|     |---- entry  # 示例代码文件夹 
|     |----README.md  # 安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/hihopeorg/is-png/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/hihopeorg/is-png/pulls) 。

## 开源协议
本项目基于 [MIT License](https://gitee.com/hihopeorg/is-png/blob/master/LICENSE) ，请自由地享受和参与开源。
